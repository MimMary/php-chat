const contain = document.querySelector('contai');
const activity =  document.querySelector('activity');
const nickname = document.querySelector('userName');
const message =  document.querySelector('message');
const userName =  document.querySelector('.userName');
const btn =  document.querySelector('.btn');

let username = prompt('enter your username');
userName.value = username;

let userId;

function getData(lastMessageId = 0) {
  $.ajax({
    type: "POST",
    url: 'index.php',
    dataType: 'json',
    data: { lastMessageId, userId },
    success: function(response) {
      response.messages.forEach(v => {
        createContent(v.nickname, v.message, v.created_at, v.messageId);
      });
      
      let onlineContain = document.querySelector('.activity-contain');
      onlineContain.innerHTML = '';
      response.users.forEach(v => {
        let activeUser = document.createElement('div');
        activeUser.setAttribute("class", 'activeUser');
        activeUser.innerHTML = v.nickname;
        onlineContain.append(activeUser);
      });
    },
  });
}

function setMessage() {
  let userNameValue = document.querySelector('.userName').value; 
  let userMessage = document.querySelector('.message').value;
  $.ajax({
    type: "POST",
    url: 'index.php',
    dataType: 'json',
    data: {
      nickname: userNameValue,
      message: userMessage,
    },
    success: function(response){
      userId = response.user_id;
      getData(getLastMessageId())
    },
  });
}

function createContent(nickname, message, created_at, messageId){
  let content = document.querySelector('.message-contain');

  let div = document.createElement('div');
  div.classList.add('messageText', username === nickname ? 'messageSent' : 'messageReceaved');
  div.setAttribute('data-id', messageId)

  let name = document.createElement('span');
  name.classList.add('name');
  name.innerHTML = nickname;
  
  let userType = document.createElement('span');
  userType.classList.add('userType');
  userType.innerHTML = message;
  
  let active = document.createElement('span');
  active.classList.add('active');
  active.innerHTML = created_at;
  
  div.append(name, active, userType);
  content.append(div);
}

getData();

let time = setInterval(() => {
  getData(getLastMessageId())
}, 5000);

function getLastMessageId() {
  let messages = document.getElementsByClassName('messageText');
  let lastMessage = messages[messages.length - 1];
  return lastMessage?.getAttribute('data-id') || 0;
}

btn.addEventListener('click' , ()  => setMessage());

