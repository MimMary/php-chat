<?php
  require_once 'connection.php';
  

  if(isset($_POST['nickname']) && isset($_POST['message'])) {
      $nickname = $_POST['nickname'];
      $message = $_POST['message'];
  
      $row = mysqli_query($conn, "SELECT * FROM users where nickname = '{$nickname}'");
  
      if($row) {
        $user = $row->fetch_assoc();
        $userId = null;
        $now = date('Y-m-d H:i:s');
  
        if($user) {
          $userId = $user['id'];
          mysqli_query($conn, "UPDATE users SET last_activity = '{$now}' WHERE id = {$userId}");
        } else {
          mysqli_query($conn, "INSERT INTO users(nickname, last_activity) VALUES('{$nickname}', '{$now}')");
          $row = mysqli_query($conn, 'SELECT LAST_INSERT_ID() as id');
          $userId = $row->fetch_assoc();
          $userId = $userId['id'];
        }
  
        $messageId = null;
  
        if($userId) {
          $row = mysqli_query($conn, "INSERT INTO messages(user_id, message, created_at) VALUES({$userId}, '{$message}', '{$now}')");
  
          $row = mysqli_query($conn, 'SELECT LAST_INSERT_ID() as id');
          $messageId = $row->fetch_assoc();
          $messageId = $messageId['id'];
        }
      }

      echo json_encode([
        'user_id' => $userId
      ]);

      exit;

  }

  if(isset($_POST['lastMessageId'])) {

    if(isset($_POST['userId']) && $_POST['userId'] > 0) {
      $userId = $_POST['userId'];
      $now = date('Y-m-d H:i:s');
      mysqli_query($conn, "UPDATE users SET last_activity = '{$now}' WHERE id = {$userId}");
    }

    $query = mysqli_query($conn, "SELECT messages.id AS messageId, `message`, `user_id`, `created_at`, nickname, last_activity
      FROM `messages`
      JOIN users ON users.id = messages.user_id
      WHERE messages.id > {$_POST['lastMessageId']}
    ");

    $messages = mysqli_fetch_all($query, MYSQLI_ASSOC);


    $query = mysqli_query($conn, "SELECT * FROM users WHERE last_activity >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
    $users = mysqli_fetch_all($query, MYSQLI_ASSOC);

    echo json_encode([
      'messages' => $messages,
      'users' => $users
    ]);

    exit;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css">
  <title>Document</title>
</head>
<body>
  <div class="chat" >
    <div class="chat__info">
      <div class="message-contain" name="contain" ></div>
      <div class="activity-contain" name="activity"></div>
    </div>
    <div class="user__info">
      <input type="text" name="name" placeholder="nickname" class="userName">
      <input type="text" name="message" placeholder="message" class="message">
      <button class="btn">send</button>
    </div>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="./javascript/script.js"></script>
</body>
</html>